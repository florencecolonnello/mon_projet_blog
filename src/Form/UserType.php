<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use App\Entity\User;


class UserType extends AbstractType{
    public function buildForm(FormBuilderInterface $builder, array $options){
        //On définit les champs ici
        $builder
            ->add('email', EmailType::class)
            ->add('password', RepeatedType::class, [
              "type" => PasswordType::class,
              "first_options" => ["label" => "Password"],
              "second_options"  => ["label" => "Confirm Password"],
              "invalid_message" => "Passwords doesn't match"
            ]);
    }

    public function configureOptions(OptionsResolver $resolver){

        //On indique quelle classe le formulaire permet de créer
        $resolver->setDefaults([
            "data_class" => User::class
        ]);
    }
}