<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Article;




class RedacArticle extends AbstractType{
    public function buildForm(FormBuilderInterface $builder, array $options){
        //On définit les champs ici
        $builder
            ->add('title', TextType::class)
            ->add('text', TextType::class)
            ->add('date', DateTimeType::class);
            //On ne met pas les boutons submit histoire de rendre
            //le formulaire le plus réutilisable possible
    }

    public function configureOptions(OptionsResolver $resolver){

        //On indique quelle classe le formulaire permet de créer
        $resolver->setDefaults([
            "data_class" => Article::class
        ]);
    }
}