<?php
//HomeController.php ??
namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ArticleRepository;
use App\Entity\Article;


class AccueilController extends Controller{
    /**
     * @Route("/accueil", name="accueil")
     */
    public function index(ArticleRepository $repo){

        $result = $repo->getAll();
        return $this->render('accueil/index.html.twig', [
            'controller_name' => 'AccueilController',
            'result'  => $result
        ]);
    }
}
