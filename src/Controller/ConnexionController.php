<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\User;

class ConnexionController extends Controller{
    /**
     * @Route("/connexion", name="connexion")
     */
    public function index(){

        $user = new User();
        //On crée le formulaire à partir de la classe Type qu'on a faite
        $form = $this->createForm(UserType::class, $user);

        //On fait la suite comme avec un formulaire normale
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            //On récupère l'instance de chien générée par le formulaire
            //avec getData() et on le donne à manger à la méthode
            //add du DogRepository qui fera persister le chien en question
            $user = $form->getData();
            dump($user);
        }

        return $this->render('connexion/index.html.twig', [
            'controller_name' => 'ConnexionController'
        ]);
    }
}
