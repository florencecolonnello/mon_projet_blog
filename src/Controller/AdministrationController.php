<?php
// ajouter un nouvel article
namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ArticleRepository;
use App\Form\RedacArticle;
use App\Entity\Article;



class AdministrationController extends Controller{
    /**
     * @Route("/administration", name="administration")
     */
    public function index(Request $request, ArticleRepository $repo){

        //On crée le formulaire à partir de la classe Type qu'on a faite
        $form = $this->createForm(RedacArticle::class);

        //On fait la suite comme avec un formulaire normale
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //On récupère l'instance de chien générée par le formulaire
            //avec getData() et on le donne à manger à la méthode
            //add du DogRepository qui fera persister le chien en question
            $repo->add($form->getData());
            //On fait une redirection lors d'un ajout réussi
            return $this->redirectToRoute("accueil");
        }
        return $this->render('administration/index.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
