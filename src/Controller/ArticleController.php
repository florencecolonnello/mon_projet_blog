<?php
//lire et modifier un article
namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ArticleRepository;
use App\Form\RedacArticle;
use App\Entity\Article;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


class ArticleController extends Controller{
    /**
     * @Route("/article/{id}", name="article")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function index(int $id, Request $request, ArticleRepository $repo){


        $article = $repo->getById($id);

        //On crée le formulaire à partir de la classe Type qu'on a faite
        $form = $this->createForm(RedacArticle::class);

        //On fait la suite comme avec un formulaire normale
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //On récupère l'instance de chien générée par le formulaire
            //avec getData() et on le donne à manger à la méthode
            //add du DogRepository qui fera persister le chien en question
            $repo->add($form->getData());
            //On fait une redirection lors d'un ajout réussi
            return $this->redirectToRoute("accueil");
        }

        return $this->render('article/index.html.twig', [
            'form' => $form->createView(),
            "article" => $article
        ]);
    }
        /**
     * @Route("/article/remove/{id}", name="remove_article")
     */
    public function remove(int $id, ArticleRepository $repo) {
        $repo->delete($id);
        return $this->redirectToRoute("home");
    }
}
