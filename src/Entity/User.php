<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;



class User{
    public $id;
    public $email;
    /**
     *  @Assert\Lenght(min=5)
     */
    public $password;

    // si on utilise un constructeur dans une entité, il faut faire en sorte qu'elle soit instanciable sans propriété.
    public function __construct(string $email = null, int $password = null, int $id = null) {
        $this->id = $id;
        $this->email = $email;
        $this->password = $password;
    }

    //Une méthode statique est une méthode qu'on peut utiliser directement sur une classe sans avoir besoin de créer d'instance. A la différence d'une méthode classique, on appelle une méthode statique avec avec `::` ex: User::fromSQL()
    /**
     * Construit un utilisateur à partir des données brutes de la base de données.
     * 
     * @param array Le tableau associatif contenant les résultats d'une requête SQL
     * @return User L'utilisateur construit à partir de données de la base de données
     */
    public static function fromSQL(array $rawData) {
        return new User(
            $rawData["id"],
            $rawData["email"],
            $rawData["password"]
        );
    }

}
