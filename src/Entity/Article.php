<?php

namespace App\Entity;

class Article{
  public $id;
  public $title;
  public $text;
  public $date;

  public function fromSQL(array $sql){
    $this->id = $sql['id'];
    $this->title = $sql['title'];
    $this->text = $sql['text'];
    $this->date = $sql['date'];
    // transforme le tableau associatif
  }
}